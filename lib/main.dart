import 'package:flutter/material.dart';
import 'package:smip_slawi_app/login.dart';
import 'package:smip_slawi_app/menu.dart';
import 'package:smip_slawi_app/welcome.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Menu(),
  ));
}
